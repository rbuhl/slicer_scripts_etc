# Copyright (c) 2020
# The PostProcessingPlugin is released under the terms of the AGPLv3 or higher.

# Cura PostProcessingPlugin
# Author:   Ryan Buhl
# Date:     July 18, 2020

# Description:  This plugin performs material changes at a given layer

from typing import List
from ..Script import Script


class MaterialChange(Script):
    _layer_keyword = ";LAYER:"

    def __init__(self):
        super().__init__()

    def getSettingDataString(self):
        # TODO: add parameters for feed rate, print speed and fan speed
        return """{
            "name":"Material Change",
            "key": "MaterialChange",
            "metadata": {},
            "version": 2,
            "settings":
            {
                "layer_number":
                {
                    "label": "Layer",
                    "description": "At what layer should material change occur. This will be before the layer starts printing.",
                    "unit": "",
                    "type": "str",
                    "default_value": "1"
                },
                "initial_retract":
                {
                    "label": "Initial Retraction",
                    "description": "Initial filament retraction distance. The filament will be retracted this amount before moving the nozzle away from the ongoing print.",
                    "unit": "mm",
                    "type": "float",
                    "default_value": 1.0
                },
                "later_retract":
                {
                    "label": "Later Retraction Distance",
                    "description": "Later filament retraction distance for removal from the printer so that you can change the filament.",
                    "unit": "mm",
                    "type": "float",
                    "default_value": 30.0
                },
                "x_position":
                {
                    "label": "X Position",
                    "description": "Extruder X position. The print head will move here for filament change.",
                    "unit": "mm",
                    "type": "float",
                    "default_value": 0
                },
                "y_position":
                {
                    "label": "Y Position",
                    "description": "Extruder Y position. The print head will move here for filament change.",
                    "unit": "mm",
                    "type": "float",
                    "default_value": 0
                },
                "extruder_index":
                {
                    "label": "Extruder to use for material swap",
                    "description": "The index (0 based) of the extruder that will be used to print the new material.",
                    "unit": "",
                    "type": "int",
                    "default_value": 0
                    
                },
                "new_material_temperature":
                {
                    "label": "New Material Temperature",
                    "description": "Hotend temperature for material to be added.",
                    "unit": "\u00b0C",
                    "type": "int",
                    "default_value": 215
                },
                "load_extrusion":
                {
                    "label": "New Material Load Length",
                    "description": "Length of new material to extrude after loading.",
                    "unit": "mm",
                    "type": "int",
                    "default_value": 30
                }
            }
        }"""

    #  Inserts the filament change g-code at specific layer numbers.
    #   \param data A list of layers of g-code.
    #   \return A similar list, with material change commands inserted.
    def execute(self, data: List[str]):
        """
            ; steps:
            ; retract filament for move;
            G1 E-1
            ; store current position ; G60
            ; move extruder to filament change position (park head?) (G0 E0 X<pos> Y<pos> Z<pos>)
            ; complete extraction
            M600 B2 E<some value> L<some_value> T<index> X<some_value> Y<some_value>
            ; set bed temperature (probably don"t need)
            ; set hotend temperature
            M109 R<some temperature>
            ; extrude new filament
            G1 E<some value>
            ; verify if more should be extruded - not sure this is possible
            ; restore saved position;
            G61 XYZ
            ; continue print
        """
        layer_nums = self.getSettingValueByKey("layer_number")
        initial_retract = self.getSettingValueByKey("initial_retract")
        later_retract = self.getSettingValueByKey("later_retract")
        x_park_pos = self.getSettingValueByKey("x_position")
        y_park_pos = self.getSettingValueByKey("y_position")
        load_extrude = self.getSettingValueByKey("load_extrusion")
        new_temperature = self.getSettingValueByKey("new_material_temperature")
        extruder_index = self.getSettingValueByKey("extruder_index")  # I think the extruder index is 0 based

        save_print_position = "G60 S1  ; save current position to slot 1 in case the Filament change command saves in slot 0\n"
        restore_saved_position = "G61 S1 XYZ  ; return to saved position\n"
        retract_before_parking = "G1 E-%d  ; retract material for extruder move\n" % initial_retract
        
        # TODO: manually move the extruder to the park location, then call M600 at that location.
        #  When the load is complete, then restore the saved position after the extruder reaches temperature to prevent
        #  "blobing"; may need to play with retraction before move as well
        extract_filament = "M600 B2 E%d L%d T%d X%.2f Y%.2f  ; extract current filament\n" % \
                           (initial_retract, later_retract, extruder_index, x_park_pos, y_park_pos)

        set_hotend_temperature = "M109 R%d T%d  ; Wait for extruder to reach %d C\n" % \
                                 (new_temperature, extruder_index, new_temperature)  # wait for hotend temperature
        load_filament = "M701 L%.2f T%d  ; Load %dmm of new filament\n" % (load_extrude, extruder_index, load_extrude)

        material_change = "; -----------------\n; Start of material change block generated by MaterialChange script\n"

        # TODO: I don't think the 'load_filament' command is actually executed after the M600
        material_change = material_change + retract_before_parking + save_print_position + extract_filament + \
            set_hotend_temperature + load_filament + restore_saved_position

        material_change = material_change + "; End of generated material change block\n; -----------------\n"

        layer_targets = layer_nums.split(",")
        if len(layer_targets) > 0:
            for layer_num in layer_targets:
                try:
                    layer_num = int(layer_num.strip()) + 1  # Needs +1 because the 1st layer is reserved for start g-code.
                except ValueError:  # Layer number is not an integer.
                    continue
                if 0 < layer_num < len(data):
                    data[layer_num] = material_change + data[layer_num]

        return data
